# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'wechat/version'

Gem::Specification.new do |spec|
  spec.name          = "wechat"
  spec.version       = Wechat::VERSION
  spec.authors       = ["Lei Lee"]
  spec.email         = ["mytake6@gmail.com"]

  spec.summary       = %q{wechat libs}
  spec.description   = %q{currently only support access_token and signature APIs.}
  spec.homepage      = "http://www.343edu.com"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rest-client", '~> 1.8'
end
