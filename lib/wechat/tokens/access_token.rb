require 'wechat/tokens/base'

module Wechat
  module Tokens
    module AccessToken
      def self.included base
        base.send :include, InstanceMethods
      end

      module InstanceMethods
        def access_token
          token = Wechat::Tokens::AccessToken::Get.refresh(self)
        end
      end

      class Get < Wechat::Tokens::Base
        def url
          "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=#{@config.specs['app_id']}&secret=#{@config.specs['app_secret']}"
        end

        def save response
          File.open(saved_file, 'w') do |f|
            f.puts "#{Time.now.to_i} #{response['access_token']}"
          end
        end
      end

    end
  end
end