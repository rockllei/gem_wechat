require 'yaml'
require 'erb'
require 'wechat/tokens/access_token'
require 'wechat/tokens/jsapi_ticket'
require 'wechat/tokens/ext'
require 'wechat/oauth'
require 'wechat/user'
require 'wechat/menu'
require 'wechat/media'
require 'wechat/message'
require 'wechat/send_message'

module Wechat
  class Config

    attr_reader :app_config_name
    attr_reader :specs
    attr_reader :output_type

    include Wechat::Tokens::AccessToken
    include Wechat::Tokens::JsapiTicket
    include Wechat::Tokens::Ext
    include Wechat::Oauth
    include Wechat::User
    include Wechat::Menu
    include Wechat::Media
    include Wechat::Message
    include Wechat::SendMessage

    def initialize app_config_name
      config_file = File.expand_path('../../../config/wechat.yml', __FILE__)
      if defined?(Rails)
        file = "#{Rails.root}/config/wechat.yml"
        config_file = file if File.exists?(file)
      end  

      config_text = ERB.new(File.read(config_file)).result
      configs = YAML.load(config_text)
      @specs = if defined?(Rails)
        configs[app_config_name][Rails.env]
      else
        configs[app_config_name]
      end

      raise "not found app name" unless @specs

      @app_config_name = app_config_name

      yield(self) if block_given?
    end

  end


end